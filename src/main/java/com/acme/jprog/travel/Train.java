package com.acme.jprog.travel;

public class Train implements Transportation {
    @Override
    public void transport(String traveller) {
        System.out.printf("Traveller %s is being transported by train", traveller);
    }

    @Override
    public int getSpeed() {
        return 200;
    }
}
