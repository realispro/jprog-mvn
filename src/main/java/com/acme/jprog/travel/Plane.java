package com.acme.jprog.travel;

public class Plane implements Transportation{

    @Override
    public void transport(String traveller) {
        System.out.printf("Traveller %s is flying%n", traveller);
    }

    @Override
    public int getSpeed() {
        return 1500;
    }
}
