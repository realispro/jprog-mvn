package com.acme.jprog.travel;

import java.util.Random;

public class TravelMain {

    public static void main(String[] args) {
        System.out.println("Let's travel! " + Transportation.description());

        String traveller = "Jan Kowalski";

        Transportation t = getTransportation();
        System.out.println("Travel vehicle will run with a speed " + t.getSpeed());
        t.transport(traveller);

        System.out.println("Done.");
    }

    public static Transportation getTransportation(){
        Random random = new Random();
        boolean byAir = random.nextBoolean();
        if(byAir){
            return new Plane();
        } else {
            return t -> System.out.println("traveller " + t + " is transported by anonymous vehicle");
            //new Train();
        }
    }
}
