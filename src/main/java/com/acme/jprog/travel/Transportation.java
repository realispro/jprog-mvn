package com.acme.jprog.travel;

/**
 * Abstract entity defining transportation service
 */
@FunctionalInterface
public interface Transportation {

    void transport(String traveller);

    // Java 8
    default int getSpeed(){
        return 0;
    }

    static String description(){
        return "Transportation is a service of moving object from point a to point b";
    }

    // Java 9
    private String fooBar(){
        return "0";
    }
}
