package com.acme.jprog.concurrency.account;


public class Account {

    private int balance = 1_000_000;

    public synchronized boolean withdraw(int amount) {

            if (balance >= amount) {
                /*
                1. read balance
                2. calculate
                3. assign
                 */
                balance = balance - amount;
                return true;
            } else {
                return false;
            }
    }

    public synchronized void deposit(int amount) {
        /*
                1. read balance
                2. calculate
                3. assign
                 */
        balance = balance + amount;
    }

    public int getBalance() {
        return balance;
    }
}
