package com.acme.jprog.concurrency.account;

import com.acme.jprog.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadsStart {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        ExecutorService es = Executors.newFixedThreadPool(2);

        System.out.println("ThreadsStart.main");
        Account account = new Account();

        es.execute(new WithdrawTask(account));
        es.execute(new DepositTask(account));

        System.out.println( "account.balance" + " = " + account.getBalance());
        System.out.println("account.done");

        es.shutdown();

    }

}
