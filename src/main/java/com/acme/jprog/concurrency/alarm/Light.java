package com.acme.jprog.concurrency.alarm;


public class Light implements Runnable{

    public void light() {

        for (int i = 0; i < 10; i++) {
            System.out.println("flash");

            try {
                Thread.sleep(1 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void run() {
        light();
    }
}
