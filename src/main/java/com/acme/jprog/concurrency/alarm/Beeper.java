package com.acme.jprog.concurrency.alarm;


public class Beeper implements Runnable{

    public void beep() {

        for (int i = 0; i < 10; i++) {
            System.out.println("beep");

            try {
                Thread.sleep(1 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        beep();
    }
}
