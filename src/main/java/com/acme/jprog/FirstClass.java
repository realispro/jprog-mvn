package com.acme.jprog;

import com.acme.jprog.calculator.Calculator;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.acme.jprog.calculator.Calculator.*;

// acme.com
public class FirstClass { // PascalCase UpperCamelCase

    public static void main(String[] args) { // main or psvm
        System.out.println("Let's start day2!");

        int dayOfWeek = 1; // camelCase lowerCamelCase
        System.out.println("today is " + dayOfWeek + " day of a week"); // sout


        boolean weekend = dayOfWeek >= 5;
        System.out.println("weekend = " + weekend); // soutv

        char c = '\t';
        System.out.println("c = [" + c + "]");

        int[] holidays = {24, 25, 26};
        int[] newHolidays = new int[4];

        System.arraycopy(holidays, 0, newHolidays, 0, holidays.length);
        newHolidays[newHolidays.length - 1] = 27;
        holidays = newHolidays;

        System.out.printf("holidays = %s\n", Arrays.toString(holidays));

        for (int h : holidays) {
            System.out.println("h = " + h);
        }

        int size = args.length>0 ? Integer.parseInt(args[0]) : 15; // ternary operator

        int[][] result = new int[size][size];

        boolean progress = true;
        for(byte x=1; x<=size && progress; x++){
            for(byte y=1; y<=size; y++){
                if(y==13){
                    //progress = false;
                    break;
                }
                result[x-1][y-1] = Calculator.multiply(x, y);
            }
        }

        for(int[] row : result){
            for(int value : row){
               System.out.print(value + "\t");
            }
            System.out.println();
        }

        String text = "Warszawa 00-950, ul. Mysliwiecka 3/5/7";
        String patternText = "\\d\\d-?\\d\\d\\d";
        Pattern pattern = Pattern.compile(patternText);
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()){
            System.out.println("found at index: " + matcher.start() + ", group:" + matcher.group());
        }


        System.out.println("Done.");
    }



    public static int multiply(int iks, int igrek){
        int result = iks * igrek;
        return result;
    }


}
