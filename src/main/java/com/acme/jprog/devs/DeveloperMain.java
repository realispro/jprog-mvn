package com.acme.jprog.devs;

import java.time.Month;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class DeveloperMain {

    public static void main(String[] args) {
        System.out.println("Let's develop!");

        Developer asia = new Developer(1,"Asia", 3, new String[]{"JS", "Angular", "CSS"});
        Developer asia2 = new Developer(2,"Asia", 4, new String[]{"JS", "Angular"});
        Developer zbyszek = new Developer(3, "Zbyszek");
        Developer joe = new Developer(4, "Joe");
        Developer alex = new Developer(5, "Alex", 2, new String[]{"Java", "Spring"});
        Developer gawel = new Developer(6, "ĄBC", 3, new String[]{"Java", "C++"});
        Developer gawel2 = new Developer(7, "ABĆ", 3, new String[]{"Java", "C++"});

        System.out.println("asia==asia2 ? " + asia.equals(asia2));

        Set<Developer> developerSet = new HashSet<>();
        developerSet.add(asia); // hash: 123
        developerSet.add(zbyszek); // hash: 321
        developerSet.add(asia2); // hash: 123
        developerSet.add(alex); // hash: 123

        System.out.println("developer set size: " + developerSet.size());

        List<Developer> developerList = new ArrayList<>();
        developerList.add(asia);
        developerList.add(0, zbyszek);
        developerList.add(joe);
        developerList.add(alex);
        developerList.add(gawel);
        developerList.add(gawel2);

        // *** Comparator - sorting
        Comparator<Developer> comparator = (d1, d2) -> d1.getGrade()-d2.getGrade();
                /*new Comparator<Developer>(){
                    @Override
                    public int compare(Developer d1, Developer d2) {
                        return d1.grade - d2.grade;
                    }
                };*/
                //new DeveloperComparator();
        developerList.sort((d1, d2) -> d1.getGrade()-d2.getGrade());


        System.out.println("developer list size: " + developerList.size());

        // *** Predicate - filtering
        /*Iterator<Developer> itr = developerList.iterator();
        while (itr.hasNext()){
            Developer dev = itr.next();
            if(dev.grade<2){
                itr.remove();
            }
        }*/

        Predicate<Developer> predicate =  d -> d.getGrade()<2;
                /*new Predicate<Developer>() {
            @Override
            public boolean test(Developer developer) {
                return developer.grade<2;
            }
        };*/
        developerList.removeIf( d -> d.getGrade()<2 );



        // *** Consumer - reviewing
        for(Developer dev : developerList){
            System.out.println("dev: " + dev.getName());
        }

        Consumer<Developer> consumer = d -> System.out.println("[consumer]dev:" + d.getName());
                /*new Consumer<Developer>() {
            @Override
            public void accept(Developer developer) {
                System.out.println("[consumer]dev:" + developer.name);
            }
        };*/
        developerList.forEach(d -> System.out.println("[consumer]dev:" + d.getName()));


        Map<Month, Developer> developerMap = new HashMap<>();
        developerMap.put(Month.JANUARY, asia);
        developerMap.put(Month.FEBRUARY, asia2);
        developerMap.put(Month.MARCH, gawel);
        developerMap.put(Month.APRIL, alex);
        developerMap.put(Month.MAY, joe);
        developerMap.put(Month.DECEMBER, zbyszek);

        Developer decemberDev = developerMap.get(Month.DECEMBER);
        System.out.println("decemberDev = " + decemberDev);





        System.out.println("Done.");
    }
}
