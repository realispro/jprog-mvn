package com.acme.jprog.devs;

import com.acme.jprog.devs.repository.InMemoryDeveloperRepository;
import com.acme.jprog.devs.repository.JdbcDeveloperRepository;

public class DeveloperRepositoryFactory {


    public static DeveloperRepository getRepository(){
        return new JdbcDeveloperRepository();
                //new InMemoryDeveloperRepository();
    }

}
