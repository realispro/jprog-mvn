package com.acme.jprog.devs;

import java.util.List;

public class DeveloperRepositoryMain {


    /**
     * - operation - getAll, getById, add
     *
     * DeveloperRepositoryMain getAll
     * DeveloperRepositoryMain getById 5
     * DeveloperRepositoryMain add Foo 2
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Let's use repository!");

        if(args.length==0){
            System.out.println("Please provide operation name: getAll/getById/add");
            return;
        }

        DeveloperRepository repository = DeveloperRepositoryFactory.getRepository();

        String operation = args[0];

        if(operation.equals("getAll")) {
            List<Developer> devs = repository.getAll();
            devs.forEach(d -> System.out.println("Developer: " + d));
        } else if(operation.equals("getById")){
            if(args.length<2){
                System.out.println("Please provide developer ID");
                return;
            }
            int id = Integer.parseInt(args[1]);
            Developer dev = repository.getById(id);
            System.out.println("Developer: " + dev);
        } else if(operation.equals("add")){
            if(args.length<3){
                System.out.println("Please provide developer data");
                return;
            }
            String name = args[1];
            int grade = Integer.parseInt(args[2]);
            Developer dev = new Developer(100, name, grade, new String[0]);
            repository.add(dev);
        }

    }

}
