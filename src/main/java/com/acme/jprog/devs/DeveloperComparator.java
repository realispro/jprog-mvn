package com.acme.jprog.devs;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class DeveloperComparator implements Comparator<Developer> {

    @Override
    public int compare(Developer d1, Developer d2) {
        // < 0 : d1, d2
        // = 0 : d1==d2
        // > 0 : d2, d1

        Collator collator = Collator.getInstance(new Locale("pl", "PL"));
        int diff = collator.compare(d1.getName(), d2.getName());
                //d1.name.compareTo(d2.name);
                //d1.grade - d2.grade;
        if(diff==0){
            return d1.getSkills().length - d2.getSkills().length;
        } else {
            return diff;
        }
    }
}
