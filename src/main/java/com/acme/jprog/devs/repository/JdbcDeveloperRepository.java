package com.acme.jprog.devs.repository;

import com.acme.jprog.devs.Developer;
import com.acme.jprog.devs.DeveloperRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcDeveloperRepository implements DeveloperRepository {

    private String driver;
    private String url;
    private String user;
    private String password;

    private static final String INSERT_DEVELOPER = "insert into developer(name, grade) values(?,?)";
    private static final String SELECT_ALL_DEVELOPERS = "select id, name, grade from developer";
    private static final String SELECT_DEVELOPER_BY_ID = "select id, name, grade from developer where id=?";

    public JdbcDeveloperRepository(){
        JdbcConfiguration jdbcConfiguration = new JdbcConfiguration();
        this.driver = jdbcConfiguration.getDriver();
        this.url = jdbcConfiguration.getUrl();
        this.user = jdbcConfiguration.getUser();
        this.password = jdbcConfiguration.getPassword();
    }



    @Override
    public List<Developer> getAll() {

        try(Connection connection = getConnection()){
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SELECT_ALL_DEVELOPERS);
            List<Developer> devs = new ArrayList<>();
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int grade = rs.getInt("grade");
                Developer dev = new Developer(id, name, grade, new String[0]);
                devs.add(dev);
            }
            return devs;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Developer getById(int id) {

        try(Connection connection = getConnection()){
            PreparedStatement statement = connection.prepareStatement(SELECT_DEVELOPER_BY_ID);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                id = rs.getInt("id");
                String name = rs.getString("name");
                int grade = rs.getInt("grade");
                Developer dev = new Developer(id, name, grade, new String[0]);
                return dev;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void add(Developer dev) {

        try(Connection connection = getConnection()){
            connection.setAutoCommit(false);
            PreparedStatement statement = connection.prepareStatement(INSERT_DEVELOPER);
            statement.setString(1, dev.getName());
            statement.setInt(2, dev.getGrade());

            statement.executeUpdate();
            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void batch(Developer dev) {

        try(Connection connection = getConnection()){
            PreparedStatement statement = connection.prepareStatement(INSERT_DEVELOPER);
            statement.setString(1, dev.getName());
            statement.setInt(2, dev.getGrade());
            statement.addBatch();
            statement.setString(1, dev.getName());
            statement.setInt(2, dev.getGrade());
            statement.addBatch();
            statement.setString(1, dev.getName());
            statement.setInt(2, dev.getGrade());
            statement.addBatch();

            statement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private Connection getConnection() {
        try {
            Class.forName(driver);
            return DriverManager.getConnection(url, user, password);

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
