package com.acme.jprog.devs.repository;

import com.acme.jprog.devs.Developer;
import com.acme.jprog.devs.DeveloperRepository;

import java.util.ArrayList;
import java.util.List;

public class InMemoryDeveloperRepository implements DeveloperRepository {

    private List<Developer> devs = new ArrayList<>();

    public InMemoryDeveloperRepository(){
        devs.add(new Developer(1, "Asia", 4, new String[]{"HTML", "JS", "CSS"}));
        devs.add(new Developer(2, "Kasia", 2, new String[]{"HTML"}));
        devs.add(new Developer(3, "Basia", 1, new String[]{"C++", "C#"}));
        devs.add(new Developer(4, "Pawel", 3, new String[]{"Java", "Spring"}));
        devs.add(new Developer(5, "Gawel", 2, new String[]{"Java"}));
    }

    @Override
    public List<Developer> getAll() {
        return devs;
    }

    @Override
    public Developer getById(int id) {
        for(Developer dev : devs){
            if(dev.getId()==id){
                return dev;
            }
        }
        return null;
    }

    @Override
    public void add(Developer dev) {
        // TODO handle id assignment
        devs.add(dev);
    }
}
