package com.acme.jprog.devs.repository;

import java.io.*;
import java.util.Properties;

public class JdbcConfiguration {

    private String driver = "com.mysql.cj.jdbc.Driver";
    private String url = "jdbc:mysql://localhost/devs";
    private String user = "root";
    private String password = "mysql";


    public JdbcConfiguration() {
        //readConfigFile();
        readPropertiesFile();
    }


    private void readPropertiesFile() {

        Properties props = new Properties();
        File file = new File("jdbc.properties");
        if (file.exists()) {
            try {
                props.load(new FileReader(file));

                driver = props.getProperty("jdbc.driver");
                url = props.getProperty("jdbc.url");
                user = props.getProperty("jdbc.user");
                password = props.getProperty("jdbc.password");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void readConfigFile() {

        File file = new File("jdbc.config");
        if (file.exists()) {

            //Reader reader = new FileReader(file);
            try (BufferedReader br = new BufferedReader(new FileReader(file));) {

                int counter = 1;
                String line;
                while ((line = br.readLine()) != null) {
                    switch (counter) {

                        case 1:
                            driver = line;
                            break;
                        case 2:
                            url = line;
                            break;
                        case 3:
                            user = line;
                            break;
                        case 4:
                            password = line;
                            break;
                    }
                    counter++;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getDriver() {
        return driver;
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
