package com.acme.jprog.devs;

import java.util.List;

public interface DeveloperRepository {

    List<Developer> getAll();

    Developer getById(int id);

    void add(Developer dev);

}
