package com.acme.jprog.devs;

import java.util.Arrays;
import java.util.Objects;

public class Developer {

    private int id;
    private String name;
    private int grade; // 0
    private String[] skills; // null

    public Developer(int id, String name, int grade, String[] skills) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.skills = skills;
    }

    public Developer(int id, String n){
        this(id, n, 1, new String[0]);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String[] getSkills() {
        return skills;
    }

    public void setSkills(String[] skills) {
        this.skills = skills;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", grade=" + grade +
                ", skills=" + Arrays.toString(skills) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Developer developer = (Developer) o;
        return id == developer.id && grade == developer.grade && Objects.equals(name, developer.name) && Arrays.equals(skills, developer.skills);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, name, grade);
        result = 31 * result + Arrays.hashCode(skills);
        return result;
    }
}
