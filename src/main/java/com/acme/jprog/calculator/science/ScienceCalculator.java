package com.acme.jprog.calculator.science;

import com.acme.jprog.calculator.Calculator;

public final class ScienceCalculator extends Calculator {

    public ScienceCalculator(double initial) {
        super(initial);
    }

    @Override
    public String getProducer() {
        return "Texas Instruments";
    }

    public double power(int operand) {
        double result = Math.pow(this.memory.getValue(), operand);
        memory.setValue(result);
        return result;
    }

}
