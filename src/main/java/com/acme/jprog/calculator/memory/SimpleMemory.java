package com.acme.jprog.calculator.memory;

import com.acme.jprog.calculator.Memory;

public class SimpleMemory implements Memory {

    private double value;

    @Override
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public double getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return "SimpleMemory{" +
                "value=" + value +
                '}';
    }
}
