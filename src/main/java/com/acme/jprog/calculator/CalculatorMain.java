package com.acme.jprog.calculator;

import com.acme.jprog.calculator.science.ScienceCalculator;

public class CalculatorMain {

    public static void main(String[] args) {
        System.out.println("Let's calculate!");

        ScienceCalculator calc1 = new ScienceCalculator(1.1);
        final Calculator calc2 = new ScienceCalculator(5.5);
        calc2.add(-5);
        //calc2 = null;

        calc1.add(4);
        calc1.multiply(3);
        calc1.divide(5);
        calc1.subtract(2);
        calc1.power(3);

        System.out.printf("calculator produced by %s, result %f\n", calc1.getProducer(), calc1.getResult());
        System.out.println("calc1=" + calc1);

        System.out.println("Done.");
    }

}
