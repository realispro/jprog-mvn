package com.acme.jprog.calculator;

import com.acme.jprog.calculator.memory.ArrayMemory;
import com.acme.jprog.calculator.memory.SimpleMemory;

public abstract class Calculator {

    protected Memory memory; //= new SimpleMemory();

    public Calculator(double initial){
        memory = new ArrayMemory(5);
        memory.setValue(initial);

    }

    public abstract String getProducer();

    public double add(double operand){
        double value = memory.getValue() + operand;
        memory.setValue(value);
        return value;
    }

    public double subtract(double operand){
        double value = memory.getValue() - operand;
        memory.setValue(value);
        return value;
    }

    public double multiply(double operand){
        double value = memory.getValue() * operand;
        memory.setValue(value);
        return value;
    }

    public double divide(double operand){
        double value = memory.getValue() / operand;
        memory.setValue(value);
        return value;
    }

    public final double getResult() { // JavaBean
        return memory.getValue();
    }

    public static int multiply(int iks, int igrek){
        int result = iks * igrek;
        return result;
    }

    @Override
    public String toString() {
        return "Calculator{" +
                "memory=" + memory +
                '}';
    }
}
