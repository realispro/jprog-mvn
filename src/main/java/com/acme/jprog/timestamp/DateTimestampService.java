package com.acme.jprog.timestamp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateTimestampService implements TimestampService{

    @Override
    public String getCurrentTimestamp() {

        int secondsFromMidnight = 121;

        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.AM_PM, Calendar.AM);

        calendar.add(Calendar.SECOND, secondsFromMidnight);

        date = calendar.getTime();

        Locale locale = new Locale("pl", "PL");
        //Locale.setDefault(locale);

        DateFormat format = new SimpleDateFormat("G X YYYY$MM$dd hh$mm$ss:SS a", locale);
                //DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);
                    // .getDateInstance(DateFormat.SHORT)
        return format.format(date); //date.toString();
    }
}
