package com.acme.jprog.timestamp;

public class TimestampMain {

    public static void main(String[] args) {
        System.out.println("Let's check time");

        TimestampService timestampService = new JavaTimeTimestampService();
        String timestamp = timestampService.getCurrentTimestamp();
        System.out.println("current time is: " + timestamp);

        System.out.println("Done.");
    }
}
