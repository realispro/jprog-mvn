package com.acme.jprog.timestamp;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class JavaTimeTimestampService implements TimestampService{
    @Override
    public String getCurrentTimestamp() {

        LocalDate date = LocalDate.now();
                //.of(2021, Month.DECEMBER, 24);
        LocalTime time = LocalTime.MIDNIGHT;
        LocalDateTime dateTime = LocalDateTime.of(date, time);
        dateTime = dateTime.plusSeconds(121);

        ZonedDateTime zonedDateTime = dateTime.atZone(ZoneId.of("Europe/Warsaw"));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("G X YYYY$MM$dd hh$mm$ss:SS a");

        return formatter.format(zonedDateTime);
                //dateTime.format(formatter);
    }
}
