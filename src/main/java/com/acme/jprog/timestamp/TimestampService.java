package com.acme.jprog.timestamp;

public interface TimestampService {

    String getCurrentTimestamp();

}
