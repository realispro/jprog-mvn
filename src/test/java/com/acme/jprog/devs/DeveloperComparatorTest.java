package com.acme.jprog.devs;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeveloperComparatorTest {

    @Test
    public void shouldKeepOrder(){

        DeveloperComparator comparator = new DeveloperComparator();

        Developer d1 = new Developer(1, "AAA", 1, new String[0]);
        Developer d2 = new Developer(2, "AAB", 1, new String[0]);

        int result = comparator.compare(d1, d2);

        assertTrue( result > 0 );
    }


    @Test
    public void shouldReverseOrder(){

        DeveloperComparator comparator = new DeveloperComparator();

        Developer d1 = new Developer(1, "AAA", 1, new String[0]);
        Developer d2 = new Developer(2, "AAB", 1, new String[0]);

        int result = comparator.compare(d2, d1);

        assertTrue( result > 0 );
    }

}